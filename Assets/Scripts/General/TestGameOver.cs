﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class TestGameOver : MonoBehaviour {
		void Update(){
			if (Input.GetKeyUp (KeyCode.O)) {
				GetComponent<GameManagerMaster> ().CallEventGameOver ();
			}
		}

	}
}