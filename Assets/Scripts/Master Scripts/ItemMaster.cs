﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class ItemMaster : MonoBehaviour {
		private PlayerMaster playerMaster;

		public delegate void GeneralEventHandler();
		public event GeneralEventHandler EventObjectThrow;
		public event GeneralEventHandler EventObjectPickup;

		public delegate void PickupActionEventHandler(Transform item);
		public event PickupActionEventHandler EventPickupAction;

		void SetInitialReferences(){
			if (Gamemanager_References._player != null) {
				playerMaster = Gamemanager_References._player.GetComponent<PlayerMaster> ();
			}
		}
		void OnEnable(){
			SetInitialReferences ();
		}

		public void CallEventObjectThrow(){
			if (EventObjectThrow != null) {
				EventObjectThrow ();
			}
			playerMaster.CallEventHandsEmpty ();
			playerMaster.CallEventInventoryChanged ();
		}
		public void CallEventObjectPickup(){
			if (EventObjectPickup != null) {
				EventObjectPickup ();

			}
			playerMaster.CallEventInventoryChanged ();
		}
		public void CallEventPickupAction(Transform item){
			if (EventPickupAction != null) {
				EventPickupAction (item);
			}
		}
		void Start(){
			SetInitialReferences ();
		}
	}
}

