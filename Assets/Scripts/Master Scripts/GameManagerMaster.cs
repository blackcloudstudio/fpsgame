﻿using UnityEngine;
using System.Collections;


namespace blackcloud {
	public class GameManagerMaster : MonoBehaviour {

		public delegate void GameManagerEventHandler();
		public event GameManagerEventHandler MenuToggleEvent;
		public event GameManagerEventHandler InventoryUIToggleEvent;
		public event GameManagerEventHandler RestartLevelEvent;
		public event GameManagerEventHandler GotoMenuSceneEvent;
		public event GameManagerEventHandler GameOverEvent;

		public bool isGameOver;
		public bool isInventoryUIOn;
		public bool isMenuOn;

		public void CallEventMenuToggle(){
			if (MenuToggleEvent != null) {
				MenuToggleEvent ();
			}
		}

		public void CallEventInventoryUIToggle(){
			if (InventoryUIToggleEvent != null) {
				InventoryUIToggleEvent ();
			}
		}

		public void RestartLevel(){
			if (RestartLevelEvent != null) {
				RestartLevelEvent ();
			}
		}
		public void CallEventMenuScene(){
			if (GotoMenuSceneEvent != null) {
				GotoMenuSceneEvent ();
			}
		}
		public void CallEventGameOver(){
			if (GameOverEvent != null) {
				isGameOver = true;
				GameOverEvent ();
			}
		}
	}
}