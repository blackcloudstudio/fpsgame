﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class MeleeMaster : MonoBehaviour {
		public delegate void GeneralEventHandler();
		public event GeneralEventHandler EventPlayerInput;
		public event GeneralEventHandler EventMeleeReset;

		public delegate void MeleeHitEventHandler(Collision hitCol, Transform hitTrans);
		public event MeleeHitEventHandler EventHit;

		public bool isInUse = false;
		public float swingRate = 0.5f;

		public void CallEventPlayerInput(){
			if (EventPlayerInput != null) {
				EventPlayerInput();
			}
		}

		public void CallEventMeleeReset(){
			if (EventMeleeReset != null) {
				EventMeleeReset();
			}
		}
		public void CallEventHit(Collision hitCol, Transform hitTrans){
			if (EventHit != null) {
				EventHit (hitCol, hitTrans);
			}
		}
	}
}