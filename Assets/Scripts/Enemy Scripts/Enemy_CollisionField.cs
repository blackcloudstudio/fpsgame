﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_CollisionField : MonoBehaviour {
		private EnemyMaster em;
		private Rigidbody rigidBodyStrikingMe;
		private int damageToApply;
		public float massRequirement = 50;
		public float speedRequirement = 5;
		private float damageFactor = 0.1f;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += disableThis;
		}
		void OnDisable(){
			em.EventEnemyDie -= disableThis;
		}
		void SetInitialReferences(){
			em = transform.root.GetComponent<EnemyMaster> ();
		}
		void OnTriggerEnter(Collider other){
			if (other.GetComponent<Rigidbody> () != null) {
				rigidBodyStrikingMe = other.GetComponent<Rigidbody> ();
				if (rigidBodyStrikingMe.mass >= massRequirement && rigidBodyStrikingMe.velocity.sqrMagnitude >= speedRequirement*speedRequirement) {
					damageToApply = (int)(damageFactor * rigidBodyStrikingMe.mass * rigidBodyStrikingMe.velocity.magnitude);
					em.CallEventEnemyDeductHealth(damageToApply);
				}
			}
		}
		void disableThis(){
			gameObject.SetActive (false);
		}
	}
}

