﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_Pursue : MonoBehaviour {
		private EnemyMaster em;
		public NavMeshAgent myNavMeshAgent;
		private float checkRate;
		private float nextChek;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += DisableThis;

		}
		void OnDisable(){
			em.EventEnemyDie -= DisableThis;
		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
			checkRate = Random.Range(0.1f, 0.2f);
			if(GetComponent<NavMeshAgent>() != null){
				myNavMeshAgent = GetComponent<NavMeshAgent>();
			}
		}
		// Update is called once per frame
		void Update () {
			if (Time.time > nextChek) {
				nextChek = Time.time + checkRate;
				tryToChaseTarget ();
			}
		}
		void tryToChaseTarget(){
			if (em.myTarget != null && myNavMeshAgent != null && !em.isNavPaused) {
				myNavMeshAgent.SetDestination (em.myTarget.position);
				if (myNavMeshAgent.remainingDistance > myNavMeshAgent.stoppingDistance) {
					em.CallEventEnemyWalking ();
					em.isOnRoute = true;
				}
			}
		}
		void DisableThis(){
			if (myNavMeshAgent != null) {
				myNavMeshAgent.enabled = false;
			}
			this.enabled = false;
		}
	}
}

