﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class SpawnerProximity : MonoBehaviour {

		public GameObject objectToSpawn;
		public int numberToSpawn;
		public float proximity;
		private float checkRate;
		private float nextCheck;
		private Transform myTransform;
		private Transform playerTransform;
		private Vector3 spawnPosition;

		void SetInitialReferences(){
			myTransform = transform;
			playerTransform = Gamemanager_References._player.transform;
			checkRate = Random.Range (0.8f,1.2f);
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
	
		// Update is called once per frame
		void Update () {
			checkDistance ();
		}
		void checkDistance(){
			if (Time.time > nextCheck) {
				nextCheck = Time.time + checkRate;
				if (Vector3.Distance (myTransform.position, playerTransform.position) < proximity) {
					spawnObjects ();
					this.enabled = false;
				}
			}
		}
		void spawnObjects(){
			for (int i = 0; i < numberToSpawn; i++) {
				spawnPosition = myTransform.position + Random.insideUnitSphere * 5;
				Instantiate (objectToSpawn, spawnPosition, myTransform.rotation);
			}
			Destroy (gameObject);
		}
	}
}

