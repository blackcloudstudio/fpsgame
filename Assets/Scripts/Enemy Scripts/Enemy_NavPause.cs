﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_NavPause : MonoBehaviour {
		private EnemyMaster em;
		private NavMeshAgent myNavMeshAgent;
		private float pauseTime = 1;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += disableThis;
			em.EventEnemyDeductHealth += PauseNavMeshAgent;
		}
		void OnDisable(){
			em.EventEnemyDie -= disableThis;
			em.EventEnemyDeductHealth -= PauseNavMeshAgent;
		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
			if(GetComponent<NavMeshAgent>() != null){
				myNavMeshAgent = GetComponent<NavMeshAgent>();
			}
		}
		void PauseNavMeshAgent(int dummy){
			if (myNavMeshAgent != null) {
				if (myNavMeshAgent.enabled) {
					myNavMeshAgent.ResetPath ();
					em.isNavPaused = true;
					StartCoroutine ("RestartNavMeshAgent");
				}
			}
		}

		IEnumerator RestartNavMeshAgent(){
			yield return new WaitForSeconds (pauseTime);
			em.isNavPaused = false;
		}

		void disableThis(){
			StopAllCoroutines ();
		}
	}
}

