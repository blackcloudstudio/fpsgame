﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_Animation : MonoBehaviour {
		private EnemyMaster em;
		private Animator myAnimator;
		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += DisableAnimator;
			em.EventEnemyWalking += SetAnimationToWalk;
			em.EventEnemyReachedNavTarget += SetAnimationToIdle;
			em.EventEnemyAttack += SetAnimationToAttack;
			em.EventEnemyDeductHealth += SetAnimationToStruck;
		}
		void OnDisable(){
			em.EventEnemyDie -= DisableAnimator;
			em.EventEnemyWalking -= SetAnimationToWalk;
			em.EventEnemyReachedNavTarget -= SetAnimationToIdle;
			em.EventEnemyAttack -= SetAnimationToAttack;
			em.EventEnemyDeductHealth -= SetAnimationToStruck;

		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
			if (GetComponent<Animator> () != null) {
				myAnimator = GetComponent<Animator> ();
			}
		}
		void SetAnimationToWalk(){
			if (myAnimator != null) {
				if (myAnimator.enabled) {
					myAnimator.SetBool ("isPursuing", true);
				}
			}
		}
		void SetAnimationToIdle(){
			if (myAnimator != null) {
				if (myAnimator.enabled) {
					myAnimator.SetBool ("isPursuing", false);
				}
			}
		}
		void SetAnimationToAttack(){
			if (myAnimator != null) {
				if (myAnimator.enabled) {
					myAnimator.SetTrigger("Attack");
				}
			}
		}
		void SetAnimationToStruck(int dummy){
			if (myAnimator != null) {
				if (myAnimator.enabled) {
					myAnimator.SetTrigger("Struck");
				}
			}
		}
		void DisableAnimator(){
			if (myAnimator != null) {
				myAnimator.enabled = false;
			}
		}
	}
}

