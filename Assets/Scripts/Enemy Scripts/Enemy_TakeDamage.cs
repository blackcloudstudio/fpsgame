﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_TakeDamage : MonoBehaviour {
		private EnemyMaster em;
		public int damageMultiplier = 1;
		public bool shouldRemoveCollider;


		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += removeThis;
		}
		void OnDisable(){
			em.EventEnemyDie -= removeThis;
		}
		void SetInitialReferences(){
			em = transform.root.GetComponent<EnemyMaster> ();
		}
		public void processDamage(int dmg){
			int damageToApply = dmg * damageMultiplier;
			em.CallEventEnemyDeductHealth (damageToApply);
		}
		void removeThis(){
			if (shouldRemoveCollider) {
				if (GetComponent<Collider> () != null) {
					Destroy(GetComponent<Collider>());
				}
				if (GetComponent<Rigidbody> () != null) {
					Destroy(GetComponent<Rigidbody>());
				}
			}
			Destroy (this);
		}
	}
}

