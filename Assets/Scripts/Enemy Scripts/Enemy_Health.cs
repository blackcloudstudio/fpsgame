﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_Health : MonoBehaviour {
		private EnemyMaster em;
		public int enemyHealth = 100;
		public float healthLow = 25f;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDeductHealth += DeductHealth;
			em.EventEnemyIncreaseHealth += IncreaseHealth;
		}
		void OnDisable(){
			em.EventEnemyDeductHealth -= DeductHealth;
			em.EventEnemyIncreaseHealth -= IncreaseHealth;
		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
		}
		void DeductHealth(int healthChange){
			enemyHealth -= healthChange;
			if (enemyHealth <= 0) {
				enemyHealth = 0;
				em.CallEventEnemyDie ();
				Destroy (gameObject, Random.Range(10, 30));
			}
			checkHealthFraction ();
		}
		void checkHealthFraction(){
			if (enemyHealth <= healthLow && enemyHealth > 1) {
				em.CallEventEnemyHealthLow ();
			} else if (enemyHealth > healthLow) {
				em.CallEventEnemyHealthRecovered ();
			}
		}
		void IncreaseHealth(int healthChange){
			enemyHealth += healthChange;
			if (enemyHealth > 100) {
				enemyHealth = 100;
			}
			checkHealthFraction ();
		}
	}
}

