﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_Attack : MonoBehaviour {
		private EnemyMaster em;
		private Transform attackTarget;
		private Transform myTransform;
		public float attackRate = 1;
		private float nextAttack;
		public float attackRange =3.5f;
		public int attackDamage = 10;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += disableThis;
			em.EventEnemySetNavTarget += setAttackTarget;
		}
		void OnDisable(){
			em.EventEnemyDie -= disableThis;
			em.EventEnemySetNavTarget -= setAttackTarget;
		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
			myTransform = transform;
		}
		void setAttackTarget(Transform targetTransform){
			attackTarget = targetTransform;
		}
		void TryToAttack(){
			if (attackTarget != null) {
				if (Time.time > nextAttack) {
					nextAttack = Time.time + attackRate;
					if (Vector3.Distance (myTransform.position, attackTarget.position) <= attackRange) {
						Vector3 lookAtVector = new Vector3 (attackTarget.position.x, myTransform.position.y, attackTarget.position.z);
						myTransform.LookAt (lookAtVector);
						em.CallEventEnemyAttack ();
						em.isOnRoute = false;

					}
				}
			}
		}
		public void OnEnemyAttack(){ //Called by hPunch animation
			if(attackTarget!=null){
				if(Vector3.Distance(myTransform.position, attackTarget.position)<=attackRange && attackTarget.GetComponent<PlayerMaster>()!=null){
					//Do the attack and cause damage:
					Vector3 toOther = attackTarget.position-myTransform.position;
					//Debug.Log(Vector3.Dot(toOther, myTransform.forward).ToString());
						if(Vector3.Dot(toOther, myTransform.forward)>0.5f){
						attackTarget.GetComponent<PlayerMaster> ().CallEventPlayerHealthDeduction (attackDamage);
						}
				}
			}
		}
		void disableThis(){
			this.enabled = false;
		}
	
		// Update is called once per frame
		void Update () {
			TryToAttack ();
		}
	}
}

