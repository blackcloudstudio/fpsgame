﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_Wander : MonoBehaviour {

		private EnemyMaster em;
		private NavMeshAgent myNavMeshAgent;
		private float checkRate;
		private float nextChek;
		private Transform myTransform;
		private float wanderRange = 10;
		private NavMeshHit navHit;
		private Vector3 wanderTarget;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += disableThis;
		}
		void OnDisable(){
			em.EventEnemyDie -= disableThis;
		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
			myTransform = transform;
			checkRate = Random.Range(0.1f, 0.2f);
			if(GetComponent<NavMeshAgent>() != null){
				myNavMeshAgent = GetComponent<NavMeshAgent>();
			}
		}
		void checkIfIShouldWander(){
			if (em.myTarget == null && em.isOnRoute && !em.isNavPaused) {
				if(RandomWanderTarget(myTransform.position, wanderRange, out wanderTarget)){
					myNavMeshAgent.SetDestination(wanderTarget);
					em.isOnRoute = true;
					em.CallEventEnemyWalking();
				}
			}
		}
		bool RandomWanderTarget(Vector3 center, float range, out Vector3 result){
			Vector3 randomPoint = center + Random.insideUnitSphere * wanderRange;
			if (NavMesh.SamplePosition (randomPoint, out navHit, 1.0f, NavMesh.AllAreas)) {
				result = navHit.position;
				return true;
			} else {
				result = center;
				return false;
			}
		
		}
		void disableThis(){
			this.enabled = false;
		}
		void Update () {
			if (Time.time > nextChek) {
				nextChek = Time.time + checkRate;
				checkIfIShouldWander ();
			}
		}
		void Start(){
			em.isOnRoute = true;
		}
	}
}

