﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_Detection : MonoBehaviour {
		private EnemyMaster em;
		private Transform myTransform;
		public Transform head;
		public LayerMask playerLayer;
		public LayerMask sightLayer;
		private float checkRate;
		private float nextCheck;
		private float detectRadius = 80;
		private RaycastHit hit;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += DisableThis;
		}
		void OnDisable(){
			em.EventEnemyDie -= DisableThis;
		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
			myTransform = transform;
			if (head == null) {
				head = myTransform;
			}
			checkRate = Random.Range(0.8f,1.2f);
		}
		void CarryOutDetection(){
			if (Time.time > nextCheck) {
				nextCheck = Time.time + checkRate;
				Collider[] colliders = Physics.OverlapSphere (myTransform.position, detectRadius, playerLayer);
				if (colliders.Length > 0) {
					foreach (Collider potTargetCol in colliders) {
						if (potTargetCol.CompareTag (Gamemanager_References._playerTag)) {
							if (canPotentialTargetBeSeen (potTargetCol.transform)) {
								break;
							}
						}
					}					
				} else {
					em.CallEventEnemyLostTarget ();
				}
			}
			
		}
		bool canPotentialTargetBeSeen(Transform potTarget){
			if (Physics.Linecast (head.position, potTarget.position, out hit, sightLayer)) {
				if (hit.transform == potTarget) {
					em.CallEventEnemySetNavTarget (potTarget);
					return true;
				} else {
					em.CallEventEnemyLostTarget ();
					return false;
				}
			} else {
				em.CallEventEnemyLostTarget ();
				return false;
			}
		}
		void DisableThis(){
			this.enabled = false;
		}
		void Update(){
			CarryOutDetection ();
		}
	}
}

