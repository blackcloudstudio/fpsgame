﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_RagdollActivation : MonoBehaviour {
		private EnemyMaster em;
		private Collider myCollider;
		private Rigidbody myRigidbody;

		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += ActivateRagdoll;
		}
		void OnDisable(){
			em.EventEnemyDie -= ActivateRagdoll;
		}
		void SetInitialReferences(){
			em = transform.root.GetComponent<EnemyMaster> ();
			if (GetComponent<Collider>() != null) {
				myCollider = GetComponent<Collider> ();
			}
			if (GetComponent<Rigidbody>() != null) {
				myRigidbody = GetComponent<Rigidbody> ();
			}
		}
		void ActivateRagdoll(){
			if (myRigidbody != null) {
				myRigidbody.isKinematic = false;
				myRigidbody.useGravity = true;
			}
			if(myCollider != null) {
				myCollider.isTrigger = false;
				myCollider.enabled = true;
			}
		}
	}
}

