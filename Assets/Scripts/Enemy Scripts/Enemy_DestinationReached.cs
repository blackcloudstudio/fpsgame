﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Enemy_DestinationReached : MonoBehaviour {
		private EnemyMaster em;
		private NavMeshAgent myNavMeshAgent;
		private float checkRate;
		private float nextChek;
		void OnEnable(){
			SetInitialReferences ();
			em.EventEnemyDie += DisableThis;
		}
		void OnDisable(){
			em.EventEnemyDie -= DisableThis;
		}
		void SetInitialReferences(){
			em = GetComponent<EnemyMaster> ();
			checkRate = Random.Range(0.3f, 0.4f);
			if(GetComponent<NavMeshAgent>() != null){
				myNavMeshAgent = GetComponent<NavMeshAgent>();
			}
		}
		void Update () {
			if (Time.time > nextChek) {
				nextChek = Time.time + checkRate;
				checkIfDestinationReached ();
			}
		}
		void checkIfDestinationReached(){
			if (em.isOnRoute) {
				if (myNavMeshAgent.remainingDistance < myNavMeshAgent.stoppingDistance) {
					em.CallEventEnemyReachedNavTarget ();
				}
			}
		}
		void DisableThis(){
			this.enabled = false;
		}
	}
}

