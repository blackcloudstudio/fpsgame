﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableSound : MonoBehaviour {
		private Destructable_Master dm;
		public float explosionVolume = 6.0f;
		public AudioClip explodingSound;

		void OnEnable(){
			SetInitialReferences ();
			dm.EventDestroyMe += playExplosionSound;
		}
		void OnDisable(){
			dm.EventDestroyMe -= playExplosionSound;
		}
		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
		}
		void playExplosionSound(){
			if (explodingSound != null) {
				AudioSource.PlayClipAtPoint (explodingSound, transform.position, explosionVolume);
			}
		}
	}
}

