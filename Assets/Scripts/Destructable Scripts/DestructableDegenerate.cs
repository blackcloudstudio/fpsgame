﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableDegenerate : MonoBehaviour {

		private Destructable_Master dm;
		private bool isHealthLow = false;
		public float degenRate = 1;
		private float nextDegenTime;
		public int healthLoss = 5;

		void OnEnable(){
			SetInitialReferences ();
			dm.EventHealthLow += healthLow;
		}
		void OnDisable(){
			dm.EventHealthLow -= healthLow;

		}
		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
		}
	
		// Update is called once per frame
		void Update () {
			checkIfHealthShouldDegenerate ();
		}
		void healthLow(){
			isHealthLow = true;
		}
		void checkIfHealthShouldDegenerate(){
			if (isHealthLow) {
				if (Time.time > nextDegenTime) {
					nextDegenTime = Time.time + degenRate;
					dm.CallEventDeductHealth (healthLoss);
				}
			}
		}
	}
}

