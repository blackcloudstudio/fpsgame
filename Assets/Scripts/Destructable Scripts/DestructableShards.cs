﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class DestructableShards : MonoBehaviour {
		private Destructable_Master dm;
		public string shardLayer = "Ignore Raycast";
		public GameObject shards;
		public bool shouldShardsDisappear;
		private float myMass = 20.0f;

		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
		}

		void OnEnable(){
			SetInitialReferences ();
			dm.EventDestroyMe += ActivateShards;
		}
		void OnDisable(){
			dm.EventDestroyMe -= ActivateShards;
		}
		void ActivateShards(){
			if (shards != null) {
				shards.transform.parent = null;
				shards.SetActive(true);
				foreach (Transform shard in shards.transform) {
					shard.tag = "Untagged";
					shard.gameObject.layer = LayerMask.NameToLayer (shardLayer);

					shard.GetComponent<Rigidbody> ().AddExplosionForce (myMass, transform.position, 40,0, ForceMode.Impulse);
					if (shouldShardsDisappear) {
						Destroy (shard.gameObject, 20);
					}
				}
			}
		}
	}
}