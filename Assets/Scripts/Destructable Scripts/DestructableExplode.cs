﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableExplode : MonoBehaviour {
		private Destructable_Master dm;
		public float explosionRange;
		public float explosionForce;
		public float destroyTime;
		private float distance;
		public int rawDamage;
		private int damageToApply;
		public Collider[] struckColliders;
		private Transform myTrans;
		private RaycastHit hit;

		void OnEnable(){
			SetInitialReferences ();
			dm.EventDestroyMe += explosionSphere;
		}
		void OnDisable(){
			dm.EventDestroyMe -= explosionSphere;
		}
		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
			myTrans = transform;
		}
		void explosionSphere(){
			myTrans.parent = null;
			GetComponent<Collider> ().enabled = false;
			struckColliders = Physics.OverlapSphere (myTrans.position, explosionRange);
			foreach (Collider col in struckColliders) {
				distance = Vector3.Distance (myTrans.position, col.transform.position);
				damageToApply = (int)Mathf.Abs ((1 - (distance / explosionRange)) * rawDamage);
				if (Physics.Linecast (myTrans.position, col.transform.position, out hit)) {
					if (hit.transform == col.transform || col.GetComponent<Enemy_TakeDamage> () != null) {
						col.SendMessage ("processDamage", damageToApply, SendMessageOptions.DontRequireReceiver);
						col.SendMessage ("CallEventPlayerHealthDeduction", damageToApply, SendMessageOptions.DontRequireReceiver);
					}
				}
				if (col.GetComponent<Rigidbody> () != null) {
					col.GetComponent<Rigidbody> ().AddExplosionForce (explosionForce, myTrans.position, explosionRange,1, ForceMode.Impulse);
				}
			}
			Destroy (gameObject, destroyTime);
		}
	}
}

