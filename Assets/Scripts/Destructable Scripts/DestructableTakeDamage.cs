﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableTakeDamage : MonoBehaviour {
		private Destructable_Master dm;

		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
		public void processDamage(int damage){
			dm.CallEventDeductHealth (damage);
		}
	}
}

