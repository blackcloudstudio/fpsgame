﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableHealth : MonoBehaviour {

		private Destructable_Master dm;
		public int health;
		private int startingHealth;
		private bool isExploding = false;

		void OnEnable(){
			SetInitialReferences ();
			dm.EventDeductHealth += DeductHealth;
		}
		void OnDisable(){
			dm.EventDeductHealth -= DeductHealth;
		}
		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
			startingHealth = health;
		}
		void DeductHealth(int healthToDeduct){
			health -= healthToDeduct;
			checkHealthLow ();
			if (health <= 0 && !isExploding) {
				isExploding = true;
				dm.CallEventDestroyMe ();
			}
		}
		void checkHealthLow(){
			if (health <= startingHealth / 2) {
				dm.CallEventHealthLow ();
			}
		}
	}
}

