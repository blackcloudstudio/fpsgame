﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructablePlayerInvUpdate : MonoBehaviour {

		private Destructable_Master dm;
		private PlayerMaster pm;

		void OnEnable(){
			SetInitialReferences ();
			dm.EventDestroyMe += ForcePlayerInventoryUpdate;
		}
		void OnDisable(){
			dm.EventDestroyMe -= ForcePlayerInventoryUpdate;
		}
		void SetInitialReferences(){
			if (GetComponent<ItemMaster> () == null) {
				Destroy (this);
			}
			if (Gamemanager_References._player != null) {
				pm = Gamemanager_References._player.GetComponent<PlayerMaster> ();
			}
			dm = GetComponent<Destructable_Master> ();
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
	
		void ForcePlayerInventoryUpdate(){
			if(pm !=null){
				pm.CallEventInventoryChanged();
			}
		}
	}
}

