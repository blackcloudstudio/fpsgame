﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableCollisionDetection : MonoBehaviour {

		private Destructable_Master dm;
		private Collider[] hitColliders;
		private Rigidbody myRigidbody;
		public float thresholdMass = 50;
		public float thresholdSpeed = 6;

		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
			if (GetComponent<Rigidbody> () != null) {
				myRigidbody = GetComponent<Rigidbody> ();
			}
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
		void OnCollisionEnter(Collision col){
			if (col.contacts.Length > 0) {
				if (col.contacts [0].otherCollider.GetComponent<Rigidbody> () != null) {
					CollisionCheck (col.contacts [0].otherCollider.GetComponent<Rigidbody> ());
				} else {
					SelfSpeedCheck ();
				}
			}
		}
		void CollisionCheck(Rigidbody otherRigidbody){
			if (otherRigidbody.mass > thresholdMass && otherRigidbody.velocity.sqrMagnitude > (thresholdSpeed * thresholdSpeed)) {
				int damage = (int)otherRigidbody.mass;
				dm.CallEventDeductHealth (damage);
				Debug.Log ("Deduct Health");
			} else {
				SelfSpeedCheck ();
			}
		}
		void SelfSpeedCheck(){
			if (myRigidbody.velocity.sqrMagnitude > (thresholdSpeed * thresholdSpeed)) {
				int damage = (int)myRigidbody.mass;
				dm.CallEventDeductHealth (damage);
			}
		}
	}
}

