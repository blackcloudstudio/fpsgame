﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableParticle : MonoBehaviour {
		private Destructable_Master dm;
		public GameObject explosionFX;
		public GameObject smokeFX;

		void OnEnable(){
			SetInitialReferences ();
			dm.EventDestroyMe += spawnFX;
		}
		void OnDisable(){
			dm.EventDestroyMe -= spawnFX;
		}
		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
		}
		void spawnFX(){
			if (explosionFX != null) {
				Instantiate (explosionFX, transform.position, Quaternion.identity);
				Instantiate (smokeFX, transform.position, Quaternion.identity);
			}
		}
	}
}

