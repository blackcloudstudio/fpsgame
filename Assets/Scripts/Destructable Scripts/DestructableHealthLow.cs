﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class DestructableHealthLow : MonoBehaviour {

		private Destructable_Master dm;
		public GameObject[] lowHealthEffect;

		void OnEnable(){
			SetInitialReferences ();
			dm.EventHealthLow += turnOnLowHealthFX;
		}
		void OnDisable(){
			dm.EventHealthLow -= turnOnLowHealthFX;
		}
		void SetInitialReferences(){
			dm = GetComponent<Destructable_Master> ();
		}
		void turnOnLowHealthFX(){
			if (lowHealthEffect.Length > 0) {
				for (int i = 0; i < lowHealthEffect.Length; i++) {
					lowHealthEffect [i].SetActive (true);
				}
			}
		}
	}
}

