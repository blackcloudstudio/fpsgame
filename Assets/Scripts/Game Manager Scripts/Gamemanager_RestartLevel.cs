﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
namespace blackcloud{
	public class Gamemanager_RestartLevel : MonoBehaviour {

		private GameManagerMaster gm;

		void OnEnable(){
			SetInitialReferences ();
			gm.RestartLevelEvent += RestartLevel;
		}

		void OnDisable(){
			gm.RestartLevelEvent -= RestartLevel;
		}

		void SetInitialReferences(){
			gm = GetComponent<GameManagerMaster> ();
		}

		void RestartLevel(){
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			//Pause Workaround see: Gamemanager_GameOver
			//Time.timeScale = 1;
		}
	}
}
