﻿using UnityEngine;
using System.Collections;

namespace blackcloud {
	public class Gamemanager_ToggleCursor : MonoBehaviour {

		private GameManagerMaster gm;
		private bool isCursorLocked = true;

		void OnEnable(){
			SetInitialReferences ();
			gm.MenuToggleEvent += ToggleCursorState;
			gm.InventoryUIToggleEvent += ToggleCursorState;
		}
		void OnDisable(){
			gm.MenuToggleEvent -= ToggleCursorState;
			gm.InventoryUIToggleEvent -= ToggleCursorState;
		}
		// Update is called once per frame
		void Update () {
			CheckIfCursorShouldBeLocked ();
		}
		void SetInitialReferences(){
			gm = GetComponent<GameManagerMaster> ();
		}
		void ToggleCursorState(){
			isCursorLocked = !isCursorLocked;
		}
		void CheckIfCursorShouldBeLocked(){
			if (isCursorLocked) {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			} else {
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}
	}
}
