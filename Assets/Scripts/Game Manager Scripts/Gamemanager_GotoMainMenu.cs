﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace blackcloud{
	public class Gamemanager_GotoMainMenu : MonoBehaviour {
		private GameManagerMaster gm;

		void OnEnable(){
			SetInitialReferences ();
			gm.GotoMenuSceneEvent += GotoMenuScene;
		}
		void OnDisable(){
			gm.GotoMenuSceneEvent -= GotoMenuScene;
		}
		void SetInitialReferences(){
			gm = GetComponent<GameManagerMaster> ();
		}
		void GotoMenuScene(){
			SceneManager.LoadScene ("Mainmenu");
		}
	}

}