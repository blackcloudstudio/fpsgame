﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class Gamemanager_ToggleInventoryUI : MonoBehaviour {
		[Tooltip("Turn inventory on or off")]
		public bool hasInventory;
		public GameObject inventoryUI;
		public string inventoryButton;

		private GameManagerMaster gm;

		void Start(){
			SetInitialReferences ();
		}
		void Update() {
			CheckForInventoryToggleRequest ();
		}
		void SetInitialReferences(){
			gm = GetComponent<GameManagerMaster> ();
			if (inventoryButton == "") {
				Debug.LogWarning ("No inventory button is set!");
				this.enabled = false;
			}
		}
		void CheckForInventoryToggleRequest(){
			if (Input.GetButtonUp (inventoryButton) && !gm.isMenuOn && !gm.isGameOver && hasInventory) {
				ToggleInventoryUI ();
			}
		}
		public void ToggleInventoryUI(){
			if (inventoryUI != null) {
				inventoryUI.SetActive (!inventoryUI.activeSelf);
				gm.isInventoryUIOn = !gm.isInventoryUIOn;
				gm.CallEventInventoryUIToggle ();
			}
		}
	}
}
