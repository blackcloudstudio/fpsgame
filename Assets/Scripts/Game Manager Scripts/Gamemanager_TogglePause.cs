﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class Gamemanager_TogglePause : MonoBehaviour {
		private GameManagerMaster gameManagerMaster;
		private bool isPaused;

		void OnEnable(){
			setInitialReferences ();
			gameManagerMaster.MenuToggleEvent += TogglePause;
			gameManagerMaster.InventoryUIToggleEvent += TogglePause;
		}

		void OnDisable(){
			gameManagerMaster.MenuToggleEvent -= TogglePause;
			gameManagerMaster.InventoryUIToggleEvent -= TogglePause;
		}
		void setInitialReferences(){
			gameManagerMaster = GetComponent<GameManagerMaster> ();
		}
		void TogglePause(){
			if (isPaused) {
				Time.timeScale = 1;
				isPaused = false;
			} else {
				Time.timeScale = 0;
				isPaused = true;
			}
		}
	}
}

