﻿using UnityEngine;
using System.Collections;

namespace blackcloud{

	public class Gamemanager_GameOver : MonoBehaviour {

		private GameManagerMaster gm;
		public GameObject panelGameOver;

		void OnEnable(){
			SetInitialReferences ();
			gm.GameOverEvent += TurnOnGameOverPanel;
		}
		void OnDisable(){
			gm.GameOverEvent -= TurnOnGameOverPanel;
			
		}
		void SetInitialReferences(){
			gm = GetComponent<GameManagerMaster> ();
		}

		void TurnOnGameOverPanel(){
			if (panelGameOver != null) {
				panelGameOver.SetActive (true);
				//Pause Workaround see: Gamemanager_GameOver
				//Time.timeScale = 0;
			}
		}
	}
}
