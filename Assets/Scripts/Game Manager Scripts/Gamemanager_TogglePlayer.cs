﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

namespace blackcloud{
	public class Gamemanager_TogglePlayer : MonoBehaviour {
		public FirstPersonController player;
		private GameManagerMaster gm;

		void OnEnable(){
			SetInitialReferences ();
			gm.MenuToggleEvent += TogglePlayer;
			gm.InventoryUIToggleEvent += TogglePlayer;
		}

		void OnDisable(){
			gm.MenuToggleEvent -= TogglePlayer;
			gm.InventoryUIToggleEvent -= TogglePlayer;
		}
		void SetInitialReferences(){
			gm = GetComponent<GameManagerMaster> ();
		}
		void TogglePlayer(){
			if(player!=null){
				player.enabled = !player.enabled;
			}
		}
	}
}
