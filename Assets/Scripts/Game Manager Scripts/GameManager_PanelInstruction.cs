﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GameManager_PanelInstruction : MonoBehaviour {
			
		public GameObject panelInstructions;
		private GameManagerMaster gm;

		void OnEnable(){
			SetInitialReferences();
			gm.GameOverEvent += TurnOffPanelInstructions;
		}
		void OnDisable(){
			gm.GameOverEvent -= TurnOffPanelInstructions;
		}
		void SetInitialReferences(){
			gm = GetComponent<GameManagerMaster>();
		}

		void TurnOffPanelInstructions() {
			if (panelInstructions != null) {
				panelInstructions.SetActive (false);
			}
		}
	}
}

