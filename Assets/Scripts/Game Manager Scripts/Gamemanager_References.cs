﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class Gamemanager_References : MonoBehaviour {

		public string playerTag;
		public static string _playerTag;

		public string enemyTag;
		public static string _enemyTag;

		public string itemTag;
		public static string _itemTag;

		public static GameObject _player;

		void OnEnable(){
			if (playerTag == "") {
				Debug.LogWarning ("Please type in the player tag name in the GameManager Ref field! "+
				"The blackcloud systems will not work without it!");
			}

			if (enemyTag == "") {
				Debug.LogWarning ("Please type in the enemy tag name in the GameManager Ref field! "+
					"The blackcloud systems will not work without it!");
			}

			if (itemTag == "") {
				Debug.LogWarning ("Please type in the item tag name in the GameManager Ref field! "+
					"The blackcloud systems will not work without it!");
			}

			_playerTag = playerTag;
			_enemyTag = enemyTag;
			_itemTag = itemTag;
			_player = GameObject.FindGameObjectWithTag(_playerTag);

		}
	}	
}