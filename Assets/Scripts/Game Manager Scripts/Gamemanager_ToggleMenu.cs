﻿using UnityEngine;
using System.Collections;

namespace blackcloud {
	public class Gamemanager_ToggleMenu : MonoBehaviour {

		private GameManagerMaster gameManagerMaster;
		public GameObject menu;

		// Use this for initialization
		void Start () {
			ToggleMenu ();
		}

		// Update is called once per frame
		void Update () {
			checkForMenuToggleRequest ();
		}

		void OnEnable(){
			setInitialReferences ();
			gameManagerMaster.GameOverEvent += ToggleMenu;
		}

		void OnDisable() {
			gameManagerMaster.GameOverEvent -= ToggleMenu;
		}

		void setInitialReferences() {
			gameManagerMaster = GetComponent<GameManagerMaster> ();
		}

		void checkForMenuToggleRequest(){
			if (Input.GetKeyUp (KeyCode.Escape) && !gameManagerMaster.isGameOver && !gameManagerMaster.isInventoryUIOn) {
				ToggleMenu ();
			}
		}

		void ToggleMenu(){
			if (menu != null) {
				menu.SetActive (!menu.activeSelf);
				gameManagerMaster.isMenuOn = !gameManagerMaster.isMenuOn;
				gameManagerMaster.CallEventMenuToggle ();
			} else {
				Debug.LogWarning ("No game menu is set in the slot!");
			}
		}
	}
}
