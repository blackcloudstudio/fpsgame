﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace blackcloud{
		public class Player_Health : MonoBehaviour {
		private GameManagerMaster gm;
		private PlayerMaster pm;
		public int playerHealth;
		public int maxHealth;
		public Text healthText;

		void OnEnable(){
			SetInitialReferences ();
		}
		void OnDisable(){
			pm.EventPlayerHealthDeduction -= DeductHealth;
			pm.EventPlayerHealthIncrease -= IncreaseHealth;
		}
		void SetInitialReferences(){
			gm = GameObject.Find ("GameManager").GetComponent<GameManagerMaster>();
			pm = GetComponent<PlayerMaster> ();
			SetUI ();
			pm.EventPlayerHealthDeduction += DeductHealth;
			pm.EventPlayerHealthIncrease += IncreaseHealth;
		}

		void Update(){
			
		}

		void Start(){
			//Comment out the line below to stop health test mode!
			//StartCoroutine (TestHealthDeduction ());
		}
		IEnumerator TestHealthDeduction(){
			yield return new WaitForSeconds (10);
			//DeductHealth (100);
			pm.CallEventPlayerHealthDeduction(50);
		}
		void DeductHealth(int healthChange){
			playerHealth = playerHealth - healthChange;
			if (playerHealth <= 0) {
				playerHealth = 0;
				gm.CallEventGameOver ();
			}
			SetUI ();
		}
		void IncreaseHealth(int increase){
			playerHealth += increase;
			if (playerHealth >= maxHealth) {
				playerHealth = maxHealth;
			}
			SetUI ();
		}
		void SetUI(){
			if (healthText != null) {
				healthText.text = playerHealth.ToString ();
			}if (playerHealth <= maxHealth/2) {
				healthText.color = Color.red;
			}
		}

	}
}

