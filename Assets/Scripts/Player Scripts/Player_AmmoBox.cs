﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace blackcloud
{
	public class Player_AmmoBox : MonoBehaviour
	{

		private PlayerMaster pm;

		[System.Serializable]
		public class AmmoTypes
		{
			public string ammoName;
			public int ammoCurrentCarry;
			public int ammoMaxQunatity;

			public AmmoTypes (string aName, int aCarry, int aMax)
			{
				ammoName = aName;
				ammoMaxQunatity = aMax;
				ammoCurrentCarry = aCarry;
			}
		}

		public List<AmmoTypes> typesOfAmmunition = new List<AmmoTypes> ();

		void OnEnable ()
		{
			SetInitialReferences ();
			pm.EventPickedUpAmmo += PickedUpAmmo;
		}

		void OnDisable ()
		{
			pm.EventPickedUpAmmo -= PickedUpAmmo;
		}

		void SetInitialReferences ()
		{
			pm = GetComponent<PlayerMaster> ();
		}

		void PickedUpAmmo (string aName, int aQuant)
		{
			for (int i = 0; i < typesOfAmmunition.Count; i++) {
				if (typesOfAmmunition [i].ammoName == aName) {
					typesOfAmmunition [i].ammoCurrentCarry += aQuant;
					if (typesOfAmmunition [i].ammoCurrentCarry > typesOfAmmunition[i].ammoMaxQunatity) {
						typesOfAmmunition [i].ammoCurrentCarry = typesOfAmmunition [i].ammoMaxQunatity;
					}
					pm.CallEventAmmoChanged ();
					break;
				}
			}
		}
	}
}

