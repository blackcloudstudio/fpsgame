﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Player_CanvasHurt : MonoBehaviour {
		public GameObject hurtCanvas;
		private PlayerMaster pm;

		public float secondsTillHide = 2;

		void OnEnable(){
			SetInitialReferences();
			pm.EventPlayerHealthDeduction += TurnOnHurtEffect;
		}
		void OnDisable(){
			pm.EventPlayerHealthDeduction -= TurnOnHurtEffect;
		}
		void SetInitialReferences(){
			pm = GetComponent<PlayerMaster> ();
		}

		void TurnOnHurtEffect(int dummy){
			if(hurtCanvas!= null){
				StopAllCoroutines();
				hurtCanvas.SetActive(true);
				StartCoroutine (ResetHurtCanvas ());
			}
		}
		IEnumerator ResetHurtCanvas(){
			yield return new WaitForSeconds (secondsTillHide);
			hurtCanvas.SetActive (false);
		}

	}
}

