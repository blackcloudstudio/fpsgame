﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class Item_ConvexMeshManagement : MonoBehaviour {
		
		private ItemMaster im;
		public MeshCollider[] meshColliders;
		public Rigidbody myRigidbody;
		public bool isSettled = true;
		private float checkRate = 0.2f;
		private float nextCheck;

		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectPickup += enableMeshConvex;
		}
		void OnDisable(){
			im.EventObjectPickup -= enableMeshConvex;
		}
	
		// Update is called once per frame
		void Update () {
			checkIfHaveSettled ();
	
		}
		void checkIfHaveSettled(){
			if (Time.time > nextCheck && !isSettled) {
				nextCheck = Time.time + checkRate;
				if(Mathf.Approximately(myRigidbody.velocity.magnitude, 0) && !myRigidbody.isKinematic){
					isSettled = true;
					disableMeshConvexAndEnableKinematic ();
				}
			}
		}
		void enableMeshConvex(){
			isSettled = false;
			if (meshColliders.Length > 0) {
				foreach (MeshCollider submesh in meshColliders) {
					submesh.convex = true;
				}
			}
		}
		void disableMeshConvexAndEnableKinematic(){
			myRigidbody.isKinematic = true;
			if (meshColliders.Length > 0) {
				foreach (MeshCollider submesh in meshColliders) {
					submesh.convex = false;
				}
			}
		}
	}
}