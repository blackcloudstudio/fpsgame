﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_UI : MonoBehaviour {
		private ItemMaster im;
		public GameObject myUI;

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectPickup += enableMyUi;
			im.EventObjectThrow += disableMyUi;
		}
		void OnDisable(){
			im.EventObjectPickup -= enableMyUi;
			im.EventObjectThrow -= disableMyUi;
		}
		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}
		void enableMyUi(){
			if (myUI != null) {
				myUI.SetActive (true);
			}
		}
		void disableMyUi(){
			if (myUI != null) {
				myUI.SetActive (false);
			}
		}
	
	}
}

