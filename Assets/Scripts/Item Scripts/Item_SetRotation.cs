﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class Item_SetRotation : MonoBehaviour {
		private ItemMaster im;
		public Vector3 itemLocalRotation;

		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectPickup += setRotationOnPlayer;
		}
		void OnDisable(){
			im.EventObjectPickup -= setRotationOnPlayer;
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
	
		void setRotationOnPlayer(){
			if (transform.root.CompareTag (Gamemanager_References._playerTag)) {
				transform.localEulerAngles = itemLocalRotation;
			}
		}
	}
}