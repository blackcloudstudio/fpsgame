﻿using UnityEngine;
using System.Collections;

namespace blackcloud
{
	public class Item_Drop : MonoBehaviour
	{
		private ItemMaster im;
		public string dropButtonName;
		private Transform myTrans;

		// Use this for initialization
		void Start ()
		{
			im = GetComponent<ItemMaster> ();
			myTrans = transform;
		}
	
		// Update is called once per frame
		void Update ()
		{
			checkForDropInput ();
		}
		void checkForDropInput(){
			if (Input.GetButtonDown(dropButtonName) && Time.timeScale >0 && myTrans.root.CompareTag(Gamemanager_References._playerTag)) {
				myTrans.parent = null;
				im.CallEventObjectThrow ();
			}
		}
	}
}
