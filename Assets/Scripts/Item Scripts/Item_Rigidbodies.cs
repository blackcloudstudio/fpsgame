﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Rigidbodies : MonoBehaviour {
		private ItemMaster im;
		public Rigidbody[] rigidbodies;

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectThrow += setIsKinematicToFalse;
			im.EventObjectPickup += setIsKinematicToTrue;
		}
		void OnDisable(){
			im.EventObjectThrow -= setIsKinematicToFalse;
			im.EventObjectPickup -= setIsKinematicToTrue;
		}
		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();

		}
		void checkIfStartsInInventory(){
			if (transform.root.CompareTag(Gamemanager_References._playerTag)){
				setIsKinematicToTrue ();
			}
		}
		void setIsKinematicToTrue(){
			if (rigidbodies.Length > 0) {
				foreach(Rigidbody rBody in rigidbodies){
					rBody.isKinematic = true;
				}
			}
		}
		void setIsKinematicToFalse(){
			if (rigidbodies.Length > 0) {
				foreach(Rigidbody rBody in rigidbodies){
					rBody.isKinematic = false;
				}
			}
		}
		void Start(){
			checkIfStartsInInventory ();
		}
	}
}

