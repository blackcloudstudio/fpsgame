﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Animator : MonoBehaviour {
		private ItemMaster im;
		public Animator myAnimator;


		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectThrow += disableAnimator;
			im.EventObjectPickup += enableAnimator;
		}
		void OnDisable(){
			im.EventObjectThrow -= disableAnimator;
			im.EventObjectPickup -= enableAnimator;
		}
		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}
		void enableAnimator(){
			if (myAnimator !=null) {
				myAnimator.enabled = true;
			}
		}
		void disableAnimator(){
			if (myAnimator !=null) {
				myAnimator.enabled = false;
			}
		}

	}
}

