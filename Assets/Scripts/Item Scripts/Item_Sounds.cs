﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Sounds : MonoBehaviour {
		private ItemMaster im;
		public float defaultVolume;
		public AudioClip throwSound;
		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectThrow += PlayThrowSound;
		}
		void OnDisable(){
			im.EventObjectThrow -= PlayThrowSound;
		}
		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}
		void PlayThrowSound(){
			if(throwSound!=null){
				AudioSource.PlayClipAtPoint(throwSound, transform.position, defaultVolume);
			}
		}
	}
}

