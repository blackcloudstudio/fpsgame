﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Ammo : MonoBehaviour {
		private ItemMaster im;
		private GameObject playerGo;
		public string ammoName;
		public int quantity;
		public bool isTriggerPickup;
		public AudioClip pickupSound;

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectPickup += takeAmmo;
		}
		void OnDisable(){
			im.EventObjectPickup -= takeAmmo;
		}
		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
			playerGo = Gamemanager_References._player;
			if (isTriggerPickup) {
				if (GetComponent<Collider> ()!=null) {
					GetComponent<Collider> ().isTrigger = true;
				}
				if (GetComponent<Rigidbody>() !=null) {
					GetComponent<Rigidbody> ().isKinematic = true;
				}
			}
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
		void OnTriggerEnter(Collider other){
			if (other.CompareTag (Gamemanager_References._playerTag) && isTriggerPickup) {
				takeAmmo ();
			}
		}
		void takeAmmo(){
			playerGo.GetComponent<PlayerMaster> ().CallEventPickedUpAmmo (ammoName, quantity);
			Destroy (gameObject);
			AudioSource.PlayClipAtPoint (pickupSound, transform.position, 1.0f);
		}
	}
}

