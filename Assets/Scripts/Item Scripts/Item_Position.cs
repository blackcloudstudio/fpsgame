﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Position : MonoBehaviour {
		private ItemMaster im;
		public Vector3 itemLocalPosition;

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectPickup += setPositionOnPlayer;
		}
		void OnDisable(){
			im.EventObjectPickup -= setPositionOnPlayer;
		}
		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}
		void setPositionOnPlayer(){
			if (transform.root.CompareTag (Gamemanager_References._playerTag)) {
				transform.localPosition = itemLocalPosition;
			}
		}
		void Start(){
			setPositionOnPlayer ();
		}
	}
}

