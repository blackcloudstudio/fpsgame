﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Colliders : MonoBehaviour {
		private ItemMaster itemMaster;
		public Collider[] colliders;
		public PhysicMaterial myPhysMat;

		void OnEnable(){
			SetInitialReferences ();

			itemMaster.EventObjectThrow += enableColliders;
			itemMaster.EventObjectPickup += disableColliders;
		}
		void OnDisable(){
			itemMaster.EventObjectThrow -= enableColliders;
			itemMaster.EventObjectPickup -= disableColliders;
		}
		void SetInitialReferences(){
			itemMaster = GetComponent<ItemMaster> ();
		}
		void checkIfStartsInInventory(){
			if(transform.root.CompareTag(Gamemanager_References._playerTag)){
				disableColliders();
			}
		}
		void enableColliders(){
			if (colliders.Length > 0) {
				foreach (Collider col in colliders) {
					col.enabled = true;
					if (myPhysMat != null) {
						col.material = myPhysMat;
					}
				}
			}
		}
		void disableColliders(){
					if (colliders.Length > 0) {
						foreach (Collider col in colliders) {
							col.enabled = false;
						}
					}
		}
		void Start(){
			checkIfStartsInInventory ();
		}
	}
}

