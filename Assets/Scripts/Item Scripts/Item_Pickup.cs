﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Pickup : MonoBehaviour {
		private ItemMaster itemMaster;
		//private Transform myTransform;

		void SetInitialReferences(){
			itemMaster = GetComponent<ItemMaster> ();
		}
		void carryOutPickupActions(Transform tParent){
			transform.SetParent (tParent);
			itemMaster.CallEventObjectPickup ();
			transform.gameObject.SetActive (false);
		}
		void OnEnable(){
			SetInitialReferences ();
			itemMaster.EventPickupAction += carryOutPickupActions;
		}
		void OnDisable(){
			itemMaster.EventPickupAction -= carryOutPickupActions;
		}
	}
}

