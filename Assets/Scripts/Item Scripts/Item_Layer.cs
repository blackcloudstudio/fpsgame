﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Layer : MonoBehaviour {
		private ItemMaster im;
		public string itemThrowLayer;
		public string itemPickupLayer;

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectPickup += setItemToPickupLayer;
			im.EventObjectThrow += setItemToThrowLayer;
		}
		void OnDisable(){
			im.EventObjectPickup -= setItemToPickupLayer;
			im.EventObjectThrow -= setItemToThrowLayer;
		}
		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}
		void setItemToThrowLayer(){
			setLayer (transform, itemThrowLayer);
		}
		void setItemToPickupLayer(){
			setLayer (transform, itemPickupLayer);
		}
		void setLayerOnEnable(){
			if (itemPickupLayer == "") {
				itemPickupLayer = "Item";
			}
			if (itemThrowLayer == "") {
				itemThrowLayer = "Item";
			}
			if (transform.root.CompareTag (Gamemanager_References._playerTag)) {
				setItemToPickupLayer ();
			} else {
				setItemToThrowLayer ();
			}
		}
		void setLayer(Transform tform, string itemLayerName){
			tform.gameObject.layer = LayerMask.NameToLayer (itemLayerName);
			foreach (Transform child in tform) {
				setLayer (child, itemLayerName);
			}
		}
		void Start(){
			setLayerOnEnable ();
		}
	}
}

