﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class Item_Throw : MonoBehaviour {
		private Transform myTransform;
		private Rigidbody myRigidbody;
		private Vector3 throwDirection;
		private ItemMaster itemMaster;

		public bool canBeThrown;
		public string throwButtonName;
		public float throwForce;

		void SetInitialReferences(){
			itemMaster = GetComponent<ItemMaster> ();
			myTransform = transform;
			myRigidbody = GetComponent<Rigidbody> ();
		}
		void Start(){
			SetInitialReferences ();
		}
		void Update(){
			checkForThrowInput ();
		}

		void checkForThrowInput(){
			if (throwButtonName != null) {
				if (Input.GetButtonDown (throwButtonName) && Time.timeScale > 0 && canBeThrown &&
				   myTransform.root.CompareTag (Gamemanager_References._playerTag)) {
					carryOutThrowActions ();
				}
			}
		}
		void carryOutThrowActions(){
			throwDirection = myTransform.parent.forward;
			myTransform.parent = null;
			itemMaster.CallEventObjectThrow ();
			hurlItem ();
		}
		void hurlItem(){
			myRigidbody.AddForce (throwDirection * throwForce, ForceMode.Impulse);
		}
	}
}

