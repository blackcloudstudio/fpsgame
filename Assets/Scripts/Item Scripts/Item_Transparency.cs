﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	public class Item_Transparency : MonoBehaviour {
		private ItemMaster im;
		public Material transparentMaterial;
		private Material primaryMaterial;

		void SetInitialReferences(){
			im = GetComponent<ItemMaster> ();
		}

		void OnEnable(){
			SetInitialReferences ();
			im.EventObjectPickup += setToTransparentMaterial;
			im.EventObjectThrow += setToPrimaryMaterial;
		}
		void OnDisable(){
			im.EventObjectPickup -= setToTransparentMaterial;
			im.EventObjectThrow -= setToPrimaryMaterial;
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
			captureStartingMaterial ();
		}
	
		void captureStartingMaterial(){
			primaryMaterial = GetComponent<Renderer> ().material;
		}
		void setToPrimaryMaterial(){
			GetComponent<Renderer> ().material = primaryMaterial;
		}
		void setToTransparentMaterial(){

				GetComponent<Renderer> ().material = transparentMaterial;
		}
	}
}