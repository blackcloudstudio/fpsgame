﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunReset : MonoBehaviour {

		private GunMaster gm;
		private ItemMaster im;

		void OnEnable(){
			SetInitialReferences ();
			if (im != null) {
				im.EventObjectThrow += resetGun;
			}
		}
		void OnDisable(){
			if (im != null) {
				im.EventObjectThrow -= resetGun;
			}
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			if (GetComponent<ItemMaster> () != null) {
				im = GetComponent<ItemMaster> ();
			}
		}
		void resetGun(){
			gm.CallEventRequestGunReset ();
		}
	
	}
}

