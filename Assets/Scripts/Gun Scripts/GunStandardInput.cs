﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunStandardInput : MonoBehaviour {
		private GunMaster gm;
		private float nextAttack;
		public float attackRate = 0.1f;
		private Transform myTransform;
		public bool isAutomatic;
		public bool hasBurstFire;
		private bool isBurstFireActive;
		public string attackButtonName;
		public string reloadButtonName;
		public string burstFireButtonName;

	
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			myTransform = transform;
			gm.isGunLoaded = true; //Gun is loaded, player can attempt shooting right away
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
		void Update(){
			checkIfWeaponShouldAttack ();
			checkForReloadRequest ();
			checkForBurstFireToggle ();
		}
		void checkIfWeaponShouldAttack(){
			if (Time.time > nextAttack && Time.timeScale > 0 && myTransform.root.CompareTag (Gamemanager_References._playerTag)) {
				if (isAutomatic && !isBurstFireActive) {
					if (Input.GetButton (attackButtonName)) {
						//Debug.Log ("Full Auto");
						attemptAttack ();
					}
				} else if (isAutomatic && isBurstFireActive) {
					if (Input.GetButtonDown (attackButtonName)) {
						//Debug.Log ("Burst Fire");
						StartCoroutine ("gunBurstFire");
					}
				} else if (!isAutomatic) {
					if (Input.GetButtonDown (attackButtonName)) {
						//Debug.Log ("Single fire");
						attemptAttack ();
					}
				}
			}
		}
		void attemptAttack(){
			nextAttack = Time.time + attackRate;
			if (gm.isGunLoaded) {
				//Debug.Log ("Shooting");
				gm.CallEventPlayerInput ();
			} else {
				gm.CallEventGunNotUsable (); //TODO: Implement click sound for empty magazine
			}
		}
		void checkForReloadRequest(){
			if (Input.GetButtonDown (reloadButtonName) && Time.timeScale > 0 && myTransform.root.CompareTag (Gamemanager_References._playerTag)) {
				gm.CallEventRequestReload ();
				Debug.Log ("Reload");
			}
		}
		void checkForBurstFireToggle(){
			if (Input.GetButtonDown (burstFireButtonName) && Time.timeScale > 0 && myTransform.root.CompareTag (Gamemanager_References._playerTag)) {
				isBurstFireActive =! isBurstFireActive;
				gm.CallEventToggleBurstFire();
				if (isBurstFireActive) {
					Debug.Log ("Burst fire mode");
				} else {
					Debug.Log ("Burst fire off");
				}
			}
		}

		IEnumerator gunBurstFire(){
			attemptAttack ();
			yield return new WaitForSeconds (attackRate);
			attemptAttack ();
			yield return new WaitForSeconds (attackRate);
			attemptAttack ();
			yield return new WaitForSeconds (attackRate);
		}
	}
}

