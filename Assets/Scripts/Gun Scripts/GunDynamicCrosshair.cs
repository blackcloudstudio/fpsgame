﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunDynamicCrosshair : MonoBehaviour {
		private GunMaster gm;
		public Transform canvasDynamicCrosshair;
		private Transform playerTransform;
		private Transform weaponCam;
		private float playerSpeed;
		private float nextCaptureTime;
		private float captureInterval = 0.5f;
		private Vector3 lastPos;
		public Animator crosshairAnimator;
		public string weaponCamName;

		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			playerTransform = Gamemanager_References._player.transform;
			findWeaponCam (playerTransform);
			setCameraOnDynamicCrosshairCanvas();
			setPlaneDistanceOnDynamicCrosshairCanvas ();
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
		}
	
		// Update is called once per frame
		void Update () {
			capturePlayerSpeed ();
			applySpeedToAnimation ();
		}
		void capturePlayerSpeed(){
			if (Time.time > nextCaptureTime) {
				nextCaptureTime = Time.time + captureInterval;
				playerSpeed = (playerTransform.position - lastPos).magnitude / captureInterval;
				lastPos = playerTransform.position;
				gm.CallEventSpeedCaptured (playerSpeed);
			}
		}
		void applySpeedToAnimation(){
			if (crosshairAnimator != null) {
				crosshairAnimator.SetFloat ("Speed", playerSpeed);
			}
			
		}
		void findWeaponCam(Transform transformToSearch){
			if (transformToSearch != null) {
				if (transformToSearch.name == weaponCamName) {
					weaponCam = transformToSearch;
					return;
				}
				foreach (Transform child in transformToSearch) {
					findWeaponCam (child);
				}
			}
			
		}
		void setCameraOnDynamicCrosshairCanvas(){
			if (canvasDynamicCrosshair != null && weaponCam != null) {
				canvasDynamicCrosshair.GetComponent<Canvas> ().renderMode = RenderMode.ScreenSpaceCamera;
				canvasDynamicCrosshair.GetComponent<Canvas> ().worldCamera = weaponCam.GetComponent<Camera> ();
			}
		}
		void setPlaneDistanceOnDynamicCrosshairCanvas(){
			if (canvasDynamicCrosshair != null) {
				canvasDynamicCrosshair.GetComponent<Canvas> ().planeDistance = 1;
			}
		}
	}
}

