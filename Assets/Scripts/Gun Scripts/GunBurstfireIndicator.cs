﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunBurstfireIndicator : MonoBehaviour {

		private GunMaster gm;
		public GameObject burstFireIndicator;

		void OnEnable(){
			SetInitialReferences ();
			gm.EventToggleBurstFire += toggleIndicator;
		}
		void OnDisable(){
			gm.EventToggleBurstFire -= toggleIndicator;
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
		}
		void toggleIndicator(){
			if (burstFireIndicator != null) {
				burstFireIndicator.SetActive (!burstFireIndicator.activeSelf);
			}
		}
	}
}

