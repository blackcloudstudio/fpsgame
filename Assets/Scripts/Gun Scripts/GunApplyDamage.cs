﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunApplyDamage : MonoBehaviour {
		private GunMaster gm;
		public int damage = 10;
		void OnEnable(){
			SetInitialReferences ();
			gm.EventShotEnemy += applyDamage;
			gm.EventShotDefault += applyDamage;
		}
		void OnDisable(){
			gm.EventShotEnemy -= applyDamage;
			gm.EventShotDefault -= applyDamage;
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
		}
		void applyDamage(Vector3 hitPos, Transform hitTrans){
			hitTrans.SendMessage ("processDamage", damage, SendMessageOptions.DontRequireReceiver);
		}
	}
}

