﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace blackcloud{
		public class GunAmmoUI : MonoBehaviour {

		public InputField currentAmmoField;
		public InputField carriedAmmoField;
		private GunMaster gm;

		void OnEnable(){
			SetInitialReferences ();
			gm.EventAmmoChanged += UpdateAmmoUI;
		}
		void OnDisable(){
			gm.EventAmmoChanged -= UpdateAmmoUI;
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
		}
		void UpdateAmmoUI(int curA, int carA){
			if (currentAmmoField != null) {
				currentAmmoField.text = curA.ToString ();
			}
			if (carriedAmmoField != null) {
				carriedAmmoField.text = carA.ToString ();
			}
		}
	}
}

