﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunMuzzleFlash : MonoBehaviour {
		private GunMaster gm;
		public ParticleSystem muzzleFlash;
		void OnEnable(){
			SetInitialReferences ();
			gm.EventPlayerInput += playMuzzleFlash;
		}
		void OnDisable(){
			gm.EventPlayerInput -= playMuzzleFlash;
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
		}
		void playMuzzleFlash(){
			if (muzzleFlash != null) {
				muzzleFlash.Play ();
			}
		}
	}
}

