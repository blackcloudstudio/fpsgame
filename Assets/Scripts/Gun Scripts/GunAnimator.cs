﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunAnimator : MonoBehaviour {
		
		private GunMaster gm;
		private Animator myAnimator;

		void OnEnable(){
			SetInitialReferences ();
			gm.EventPlayerInput += playShootAnimation;
		}
		void OnDisable(){
			gm.EventPlayerInput -= playShootAnimation;
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			if (GetComponent<Animator>()!=null) {
				myAnimator = GetComponent<Animator> ();
			}
		}
		void playShootAnimation(){
			if (myAnimator != null) {
				myAnimator.SetTrigger ("Shoot");
			}
		}
	}
}

