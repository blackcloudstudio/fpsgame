﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunApplyForce : MonoBehaviour {
		private GunMaster gm;
		private Transform myTransform;
		public float forceToApply = 300;

		void OnEnable(){
			SetInitialReferences ();
			gm.EventShotDefault += applyForce;
		}
		void OnDisable(){
			gm.EventShotDefault -= applyForce;
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			myTransform = transform;
		}
		void applyForce(Vector3 hitPos, Transform hitTrans){

			if (hitTrans.GetComponent<Rigidbody> () != null) {
				hitTrans.GetComponent<Rigidbody>().AddForce(-myTransform.forward * forceToApply, ForceMode.Impulse);
			}
		}
	}
}

