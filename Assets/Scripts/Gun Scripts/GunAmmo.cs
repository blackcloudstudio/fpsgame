﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunAmmo : MonoBehaviour {

		private PlayerMaster pm;
		private GunMaster gm;
		private Player_AmmoBox ammoBox;
		private Animator myAnimator;

		public int clipSize;
		public int currentAmmo;
		public string AmmoName;
		public float reloadTime;

		void OnEnable(){
			SetInitialReferences ();
			startingSanityCheck ();
			checkAmmoStatus ();

			gm.EventPlayerInput += DeductAmmo;
			gm.EventPlayerInput += checkAmmoStatus;
			gm.EventRequestReload += TryToReload;
			//gm.EventGunNotUsable += TryToReload; //Auto Reload
			gm.EventRequestGunReset += ResetGunReloading;
			if (pm != null) {
				pm.EventAmmoChanged += UIAmmoUpdateRequest;
			}
			if (ammoBox != null) {
				StartCoroutine(UpdateAmmoUIWhenEnabling());
			}

		}
		void OnDisable(){
			gm.EventPlayerInput -= DeductAmmo;
			gm.EventPlayerInput -= checkAmmoStatus;
			gm.EventRequestReload -= TryToReload;
			//gm.EventGunNotUsable -= TryToReload; //AutoReload
			gm.EventRequestGunReset -= ResetGunReloading;
			if (pm != null) {
				pm.EventAmmoChanged -= UIAmmoUpdateRequest;
			}
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			if (GetComponent<Animator> () != null) {
				myAnimator = GetComponent<Animator> ();
			}
			if (Gamemanager_References._player != null) {
				pm = Gamemanager_References._player.GetComponent<PlayerMaster> ();
				ammoBox = Gamemanager_References._player.GetComponent<Player_AmmoBox> ();
			}
		}
		// Use this for initialization
		void Start () {
			SetInitialReferences ();
			StartCoroutine(UpdateAmmoUIWhenEnabling());
			if (pm != null) {
				pm.EventAmmoChanged -= UIAmmoUpdateRequest;
			}
			if (ammoBox != null) {
				StartCoroutine(UpdateAmmoUIWhenEnabling());
			}
		}
		void DeductAmmo(){
			currentAmmo--;
			UIAmmoUpdateRequest ();
		}
		void TryToReload(){
			for (int i = 0; i < ammoBox.typesOfAmmunition.Count; i++) {
				if (ammoBox.typesOfAmmunition [i].ammoName == AmmoName) {
					if (ammoBox.typesOfAmmunition [i].ammoCurrentCarry > 0 && currentAmmo != clipSize && !gm.isReloading) {
						gm.isReloading = true;
						gm.isGunLoaded = false;
						if (myAnimator != null) {
							myAnimator.SetTrigger ("Reload");
						} else {
							StartCoroutine (ReloadWithoutAnimation());
						}
					}break;
				}
			}
		}
		void checkAmmoStatus(){
			if (currentAmmo <= 0) {
				currentAmmo = 0;
				gm.isGunLoaded = false;
			} else if (currentAmmo >= 0) {
				gm.isGunLoaded = true;
			}
		}
		void startingSanityCheck(){
			if (currentAmmo > clipSize) {
				currentAmmo = clipSize;
			}
		}
		void UIAmmoUpdateRequest(){
			for (int i = 0; i < ammoBox.typesOfAmmunition.Count; i++) {
				if (ammoBox.typesOfAmmunition [i].ammoName == AmmoName) {
					gm.CallEventAmmoChanged (currentAmmo, ammoBox.typesOfAmmunition[i].ammoCurrentCarry);
					break;
				}
			}
		}
		void ResetGunReloading(){
			gm.isReloading = false;
			checkAmmoStatus ();
			UIAmmoUpdateRequest ();
		}
		public void OnReloadComplete(){ //Called by reload animation
			//Attempt to add ammo to current ammo
			for(int i=0;i<ammoBox.typesOfAmmunition.Count;i++){
				if (ammoBox.typesOfAmmunition [i].ammoName == AmmoName) {
					int ammoTopUp = clipSize - currentAmmo;
					if (ammoBox.typesOfAmmunition [i].ammoCurrentCarry >= ammoTopUp) {
						currentAmmo += ammoTopUp;
						ammoBox.typesOfAmmunition [i].ammoCurrentCarry -= ammoTopUp;
					}else if (ammoBox.typesOfAmmunition [i].ammoCurrentCarry < ammoTopUp&&ammoBox.typesOfAmmunition[i].ammoCurrentCarry !=0) {
						currentAmmo += ammoBox.typesOfAmmunition [i].ammoCurrentCarry;
						ammoBox.typesOfAmmunition [i].ammoCurrentCarry = 0;
					}
					break;
				}
			}
			ResetGunReloading ();
		}
		IEnumerator ReloadWithoutAnimation(){
			yield return new WaitForSeconds (reloadTime);
			OnReloadComplete ();
		}
		IEnumerator UpdateAmmoUIWhenEnabling(){
			yield return new WaitForSeconds (0.05f); //This is a fudge factor to ensure the UI update when changing weapons
			UIAmmoUpdateRequest();
		}
	}
}

