﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunSounds : MonoBehaviour {

		private GunMaster gm;
		private Transform myTransform;
		public float shootVolume =0.2f;
		public float reloadVolume=0.5f;
		public AudioClip[] shootSound;
		public AudioClip reloadSound;

		void OnEnable(){
			SetInitialReferences ();
			gm.EventPlayerInput += playShootSound;

		}
		void OnDisable(){
			gm.EventPlayerInput += playShootSound;

		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			myTransform = transform;
		}
		void playShootSound(){
			if (shootSound.Length > 0) {
				int index = Random.Range (0, shootSound.Length);
				AudioSource.PlayClipAtPoint (shootSound [index], myTransform.position, shootVolume);
			}
		}
		public void playReloadSound (){ //Called by the reload Animation
			if(reloadSound!=null){
				AudioSource.PlayClipAtPoint (reloadSound, myTransform.position, reloadVolume);
			}
		} 
	}
}

