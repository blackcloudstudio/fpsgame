﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
		public class GunHitEffects : MonoBehaviour {
		private GunMaster gm;
		public GameObject defaultHitEffect;
		public GameObject enemyHitEffect;

		void OnEnable(){
			SetInitialReferences ();
			gm.EventShotDefault += spawnDefaultHitEffect;
			gm.EventShotEnemy += spawnEnemyHitEffect;
		}
		void OnDisable(){
			gm.EventShotDefault -= spawnDefaultHitEffect;
			gm.EventShotEnemy -= spawnEnemyHitEffect;
			
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
		}
		void spawnDefaultHitEffect(Vector3 hitPos, Transform hitTrans){
			if (defaultHitEffect != null) {
				Instantiate (defaultHitEffect, hitPos, Quaternion.identity);
			}
		
		}
		void spawnEnemyHitEffect(Vector3 hitPos, Transform hitTrans){
			if (enemyHitEffect != null) {
				Instantiate (enemyHitEffect, hitPos, Quaternion.identity);
			}
		}
	}
}

