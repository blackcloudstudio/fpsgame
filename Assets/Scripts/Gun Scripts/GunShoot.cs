﻿using UnityEngine;
using System.Collections;

namespace blackcloud{
	
		public class GunShoot : MonoBehaviour {

		private GunMaster gm;
		private Transform myTransform;
		private Transform camTransform;
		private RaycastHit hit;
		public float WeaponRange = 400;
		private float offsetFactor = 7;
		private Vector3 startPositon;

		void OnEnable(){
			SetInitialReferences ();
			gm.EventPlayerInput += openFire;
			gm.EventSpeedCaptured += setStartOfShootingPosition;
		}
		void OnDisable(){
			gm.EventPlayerInput -= openFire;
			gm.EventSpeedCaptured -= setStartOfShootingPosition;
		}
		void SetInitialReferences(){
			gm = GetComponent<GunMaster> ();
			myTransform = transform;
			camTransform = myTransform.parent;
		}
		void openFire(){
			//Debug.Log ("Open Fire");
			if (Physics.Raycast (camTransform.TransformPoint (startPositon), camTransform.forward, out hit, WeaponRange)) {
				gm.CallEventShotDefault (hit.point, hit.transform);
				if (hit.transform.CompareTag (Gamemanager_References._enemyTag)) {
					//Debug.Log ("Enemy shot");
					gm.CallEventShotEnemy (hit.point, hit.transform);
				}
			}
			
		}
		void setStartOfShootingPosition(float playerSpeed){
			float offset = playerSpeed / offsetFactor;
			startPositon = new Vector3 (Random.Range(-offset, offset), Random.Range(-offset, offset), 1);
		}
	}
}

